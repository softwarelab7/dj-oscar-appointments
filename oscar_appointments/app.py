from django.conf.urls import url

from oscar.core.application import Application

from . import views


class ScheduleDashboardApplication(Application):
    name = 'schedules'

    # Stylists have to be Partners. Staff can only see Admin Schedule
    permissions_map = _map = {
        'create': ['partner.dashboard_access'],
        'detail': (['is_staff'], ['partner.dashboard_access']),
        'edit': ['partner.dashboard_access'],
        'delete': ['partner.dashboard_access'],
        'appointment_padding': ['is_staff'],
        'appontment_detail': (['is_staff'], ['partner.dashboard_access']),
        'available-schedules-interval': ['is_staff'],
        'admin-work-schedules': ['is_staff'],
        'work-schedules': ['partner.dashboard_access'],
        'work-schedule-create': ['partner.dashboard_access'],
        'admin-work-schedule-create': ['is_staff'],
        'new-admin-work-schedule-create': ['is_staff'],
        'admin-per-stylist-work-schedules': ['is_staff'],
        'admin-work-schedule-delete': (['is_staff'], ['partner.dashboard_access']),
        'admin-work-schedule-update': (['is_staff'], ['partner.dashboard_access']),
        'now_padding': ['is_staff'],
        'color-list': ['is_staff'],
        'color-create': ['is_staff'],
        'color-edit': ['is_staff'],
    }

    list_view = views.UnavailableScheduleListView
    create_view = views.UnavailableScheduleFormView
    update_view = views.UnavailableScheduleUpdateView
    delete_view = views.UnavailableScheduleDeleteView
    appointment_padding_view = views.AppointmentPaddingView
    appointment_detail_view = views.AppointmentDetailView
    available_schedules_interval_view = views.AvailableSchedulesIntervalView
    admin_work_schedules_view = views.StylistWorkScheduleAdminView
    admin_per_stylist_work_schedules_view = views.PerStylistWorkScheduleAdminView
    work_schedules_view = views.StylistWorkScheduleView
    new_work_schedules_create_view = views.NewStylistWorkScheduleCreateView
    work_schedules_delete_view = views.StylistWorkScheduleDeleteView
    work_schedules_update_view = views.StylistWorkScheduleUpdateView
    work_schedules_create_view = views.StylistWorkScheduleCreateView
    now_padding_view = views.CurrentDatetimePaddingView
    color_list_view = views.ColorListView
    color_update_view = views.ColorUpdateView
    color_create_view = views.ColorCreateView

    def get_urls(self):
        urlpatterns = [
            url(r'^$', self.list_view.as_view(), name='list'),
            url(r'^create/$', self.create_view.as_view(),
                name='create'),
            url(r'^(?P<pk>\d+)/edit/$', self.update_view.as_view(),
                name='edit'),
            url(r'^(?P<pk>\d+)/delete/$', self.delete_view.as_view(),
                name='delete'),
            url(r'^appointment/padding/$', self.appointment_padding_view.as_view(),
                name='appointment_padding'),
            url(r'^appointment/now_padding/$', self.now_padding_view.as_view(),
                name='now_padding'),
            url(r'^appointment/(?P<pk>\d+)/$', self.appointment_detail_view.as_view(),
                name='appointment_detail'),
            url(r'^colors/$', self.color_list_view.as_view(),
                name='color-list'),
            url(r'^colors/create/$', self.color_create_view.as_view(),
                name='color-create'),
            url(r'^colors/(?P<pk>\d+)/$', self.color_update_view.as_view(),
                name='color-edit'),
            url(r'^interval/$', self.available_schedules_interval_view.as_view(),
                name='available-schedules-interval'),
            url(r'^work_schedules/$', self.work_schedules_view.as_view(),
                name='work-schedules'),
            url(r'^work_schedules/(?P<pk>\d+)/create/$', self.work_schedules_create_view.as_view(admin=False),
                name='work-schedule-create'),
            url(r'^work_schedules/create/$', self.new_work_schedules_create_view.as_view(),
                name='new-admin-work-schedule-create'),
            url(r'^work_schedules/(?P<pk>\d+)/$', self.work_schedules_update_view.as_view(),
                name='admin-work-schedule-update'),
            url(r'^work_schedules/(?P<pk>\d+)/delete/$', self.work_schedules_delete_view.as_view(),
                name='admin-work-schedule-delete'),
            url(r'^work_schedules/admin/(?P<pk>\d+)/create/$', self.work_schedules_create_view.as_view(admin=True),
                name='admin-work-schedule-create'),
            url(r'^work_schedules/admin/$', self.admin_work_schedules_view.as_view(),
                name='admin-work-schedules'),
            url(r'^work_schedules/admin/(?P<pk>\d+)/$', self.admin_per_stylist_work_schedules_view.as_view(admin=True),
                name='admin-per-stylist-work-schedules'),
            url(r'^work_schedules/stylist/(?P<pk>\d+)/$', self.admin_per_stylist_work_schedules_view.as_view(admin=False),
                name='per-stylist-work-schedules'),
        ]
        return self.post_process_urls(urlpatterns)


application = ScheduleDashboardApplication()
