# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('oscar_appointments', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AppointmentPadding',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('minutes', models.PositiveIntegerField(default=15, help_text='padding before and after appointment in minutes', verbose_name='Minutes')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CurrentDateTimePadding',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('minutes', models.PositiveIntegerField(default=60, help_text='padding in minutes from current datetime for booking appointments', verbose_name='Minutes')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
