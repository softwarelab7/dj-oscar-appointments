# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management import call_command
from django.db import migrations, models


def load_fixture(apps, schema_editor):
    call_command('loaddata', 'color', app_label='oscar_appointments')


def unload_fixture(apps, schema_editor):
    Color = apps.get_model("oscar_appointments", "Color")
    Color.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('oscar_appointments', '0002_appointmentpadding_currentdatetimepadding'),
    ]

    operations = [
        migrations.RunPython(load_fixture, reverse_code=unload_fixture),
    ]
