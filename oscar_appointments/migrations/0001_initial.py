# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import timezone_field.fields
import django.core.validators
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0008_auto_20160304_1652'),
        ('order', '0004_auto_20160111_1108'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('payment', '0002_auto_20141007_2032'),
        ('address', '0003_auto_20160804_0710'),
    ]

    operations = [
        migrations.CreateModel(
            name='Appointment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_updated', models.DateTimeField(auto_now=True)),
                ('start', models.DateTimeField()),
                ('end', models.DateTimeField()),
                ('padded_start', models.DateTimeField()),
                ('padded_end', models.DateTimeField()),
                ('status', models.CharField(default=b'New', max_length=20, verbose_name='Status', choices=[(b'New', 'New'), (b'Finished', 'Finished'), (b'Cancelled', 'Cancelled')])),
                ('cancellation_datetime', models.DateTimeField(null=True, blank=True)),
                ('uuid', django_extensions.db.fields.UUIDField(unique=True, editable=False, blank=True)),
                ('preferred_stylist', models.TextField(null=True, blank=True)),
                ('hair_condition', models.TextField(null=True, blank=True)),
                ('amount_refunded', models.DecimalField(null=True, verbose_name='Amount Refunded', max_digits=12, decimal_places=2, blank=True)),
                ('timezone', timezone_field.fields.TimeZoneField(blank=True, null=True)),
                ('four_hours_prior_notification_sent', models.DateTimeField(null=True, blank=True)),
                ('one_hour_prior_notification_sent', models.DateTimeField(null=True, blank=True)),
                ('started_notification_sent', models.DateTimeField(null=True, blank=True)),
                ('rating_notification_sent', models.DateTimeField(null=True, blank=True)),
                ('day_prior_email_sent', models.DateTimeField(null=True, blank=True)),
                ('day_prior_notification_sent', models.DateTimeField(null=True, blank=True)),
                ('gratuity_included', models.BooleanField(default=True)),
                ('gratuity_percentage', models.IntegerField(default=15)),
                ('address', models.ForeignKey(blank=True, to='address.UserAddress', null=True)),
                ('look', models.ForeignKey(to='catalogue.Product')),
                ('order', models.ForeignKey(to='order.Order')),
                ('payment_method', models.ForeignKey(to='payment.Bankcard')),
                ('style', models.ForeignKey(to='catalogue.Category')),
                ('stylist', models.ForeignKey(related_name='appointments', verbose_name='Stylists', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('stylists', models.ManyToManyField(related_name='bookable_appointments', verbose_name='Stylists', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Color',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(help_text="Hexadecimal color code with format: '#XXXXXX'", unique=True, max_length=7, verbose_name='code')),
                ('stylist', models.OneToOneField(related_name='color', null=True, blank=True, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='StylistWorkSchedule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('weekday', models.IntegerField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(6)])),
                ('start', models.TimeField()),
                ('end', models.TimeField()),
                ('uuid', django_extensions.db.fields.UUIDField(unique=True, editable=False, blank=True)),
                ('stylist', models.ForeignKey(related_name='work_schedules', verbose_name='Stylists', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['weekday', 'start'],
            },
        ),
        migrations.CreateModel(
            name='UnavailableSchedule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256, verbose_name='title')),
                ('start', models.DateTimeField()),
                ('end', models.DateTimeField()),
                ('uuid', django_extensions.db.fields.UUIDField(unique=True, editable=False, blank=True)),
                ('tz_offset', models.IntegerField(default=0)),
                ('stylist', models.ForeignKey(related_name='unavailable_scheds', verbose_name='Stylists', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-start'],
                'verbose_name': 'Unavailable Schedule',
            },
        ),
    ]
