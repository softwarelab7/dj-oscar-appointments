import datetime
import pytz
from braces.views import JSONResponseMixin
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import Http404
from django.shortcuts import redirect
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from django.views.generic import (CreateView, DetailView, FormView, ListView,
                                  UpdateView, DeleteView)
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import FormMixin

from oscar.core.compat import get_user_model

from dj_address_utils.utils import get_address_timezone
from zazzazu.basket.utils import (get_skills_from_product,
                                  get_not_in_coverage_stylists,
                                  get_stockrecords_from_options,
                                  order_weighted_stockrecords_randomly,
                                  try_assigning_stockrecords)
from zs_core.mixins import AppointmentPhoneNumberMixin
from zs_core.utils import get_normalized_tz
from .forms import (AddUnavailableScheduleForm,
                    StylistWorkScheduleForm, StylistWorkScheduleCreateForm,
                    StylistWorkScheduleAdminForm,
                    NewStylistWorkScheduleCreateForm,
                    TimezoneForm)
from .models import (Appointment, AppointmentPadding,
                     AvailableSchedulesInterval, Color,
                     CurrentDateTimePadding, StylistWorkSchedule,
                     UnavailableSchedule)

User = get_user_model()


def filter_schedules(queryset, user):
    """
    Restrict the queryset to products the given user has access to.
    A staff user is allowed to access all schedules for viewing.
    A non-staff user is only allowed access to a schedule if it's theirs.
    """
    if user.is_staff:
        return queryset

    if hasattr(queryset.first(), 'stylists'):
        return queryset.filter(stylists__in=[user.pk]).distinct()
    else:
        return queryset.filter(Q(stylist__pk=user.pk))


class UnavailableScheduleListView(ListView):
    context_object_name = 'schedules'
    model = UnavailableSchedule
    template_name = 'oscar_appointments/schedule_list.html'

    def get_queryset(self):
        return filter_schedules(UnavailableSchedule.objects.all(), self.request.user)

    def get_context_data(self, **kwargs):
        ctx = super(UnavailableScheduleListView, self).get_context_data(**kwargs)
        ctx['title'] = _('Unavailable Schedules')
        return ctx


class UnavailableScheduleFormView(FormView):
    template_name = 'oscar_appointments/schedule_form.html'
    form_class = AddUnavailableScheduleForm

    def get_context_data(self, **kwargs):
        ctx = super(UnavailableScheduleFormView, self).get_context_data(
            **kwargs)
        ctx['title'] = _('Create Unavailable Schedule')
        ctx['user'] = self.request.user
        return ctx

    def get_form_kwargs(self):
        kwargs = super(UnavailableScheduleFormView, self).get_form_kwargs()
        kwargs.update({'default_address': self.request.user.default_address.first()})
        return kwargs

    def get_initial(self):
        date = self.request.GET.get('date', None)
        if date is None:
            return self.initial.copy()
        else:
            return {'start_date': date}

    def get_success_url(self):
        return reverse('schedules:list')

    def form_valid(self, form):
        stylist = self.request.user
        start = form.cleaned_data.get('start')
        stylist.unavailable_scheds.create(
            title=form.cleaned_data.get('title'),
            start=start,
            end=form.cleaned_data.get('end'),
            timezone=start.tzinfo)
        return redirect(self.get_success_url())


class UnavailableScheduleUpdateView(SingleObjectMixin, FormView):
    template_name = 'oscar_appointments/schedule_form.html'
    form_class = AddUnavailableScheduleForm

    def get_context_data(self, **kwargs):
        queryset = self.get_queryset()
        self.object = super(UnavailableScheduleUpdateView, self).get_object(
            queryset)
        ctx = super(UnavailableScheduleUpdateView, self).get_context_data(
            **kwargs)
        ctx['title'] = _("Edit Unavailable Schedule '%s'" % (
            self.object.title))
        return ctx

    def get_queryset(self):
        # can only edit unavailable schedule if stylist or staff
        if self.request.user.is_staff:
            return UnavailableSchedule.objects.all()
        else:
            return UnavailableSchedule.objects.filter(
                stylist__pk=self.request.user.pk,
                start__gte=datetime.datetime.now(pytz.utc))

    def get_initial(self):
        queryset = self.get_queryset()
        self.object = super(UnavailableScheduleUpdateView, self).get_object(
            queryset)

        start = self.object.start.astimezone(self.object.timezone)
        end = self.object.end.astimezone(self.object.timezone)
        initial_values = {
            'title': self.object.title,
            'start_date': start.date(),
            'start_time': start.time(),
            'end_date': end.date(),
            'end_time': end.time()
        }
        return initial_values

    def get_success_url(self):
        return reverse('schedules:list')

    def form_valid(self, form):
        schedule = self.object
        schedule.title = form.cleaned_data.get('title')
        schedule.start = form.cleaned_data.get('start')
        schedule.end = form.cleaned_data.get('end')
        schedule.save()
        return redirect(self.get_success_url())


class UnavailableScheduleDeleteView(DeleteView):
    template_name = 'oscar_appointments/schedule_delete.html'
    model = UnavailableSchedule
    context_object_name = 'schedule'

    def get_queryset(self):
        # can only edit unavailable schedule if stylist or staff
        if self.request.user.is_staff:
            return UnavailableSchedule.objects.all()
        else:
            return UnavailableSchedule.objects.filter(
                stylist__pk=self.request.user.pk,
                start__gte=datetime.datetime.now(pytz.utc))

    def get_context_data(self, **kwargs):
        ctx = super(UnavailableScheduleDeleteView, self).get_context_data(**kwargs)
        ctx['title'] = _("Delete unavailable schedule?")
        return ctx

    def get_success_url(self):
        msg = _("Deleted Unavailable Schedule")
        messages.success(self.request, msg)
        return reverse('schedules:list')


class AppointmentDetailView(JSONResponseMixin, AppointmentPhoneNumberMixin, DetailView):
    template_name = 'schedules/schedule_appointment_detail.html'
    model = Appointment
    context_object_name = 'appointment'

    def get_context_data(self, **kwargs):
        ctx = super(AppointmentDetailView, self).get_context_data(
            **kwargs)
        ctx['title'] = _("Detail for '%s'" % (self.object.order))
        return ctx

    def get_queryset(self):
        return filter_schedules(Appointment.objects.all(),
                                self.request.user)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        if request.is_ajax():
            context['phone_number'] = self.get_phone_number(self.object)
            data = {'content': render_to_string(
                'schedules/_appointment_popover.html',
                context_instance=RequestContext(request, context))}
            return self.render_json_response(data)
        return self.render_to_response(context)


class AppointmentPaddingView(UpdateView):
    model = AppointmentPadding
    fields = ('minutes',)

    def get_object(self):
        try:
            padding = AppointmentPadding.objects.first()
        except AppointmentPadding.DoesNotExist:
            padding = AppointmentPadding.objects.create()
        return padding

    def get_context_data(self, **kwargs):
        ctx = super(AppointmentPaddingView, self).get_context_data(
            **kwargs)
        ctx['title'] = _("Appointment Padding")
        return ctx

    def get_success_url(self):
        msg = _("Appointment padding updated to {} minutes".format(self.object.minutes))
        messages.success(self.request, msg)
        return reverse('dashboard:index')


class StylistWorkScheduleAdminView(JSONResponseMixin, FormMixin, ListView):
    model = StylistWorkSchedule
    context_object_name = 'stylists'
    template_name = 'oscar_appointments/work_schedules_admin.html'
    form_class = StylistWorkScheduleAdminForm

    def get_queryset(self):
        stylist = self.request.GET.get('stylist', None)
        active_stylists = User.active_stylists.all()
        active_work_schedules = StylistWorkSchedule.objects.filter(
            stylist__in=active_stylists)
        if stylist:
            stylist_pks = [stylist]
        else:
            # make sure that only active stylist pks
            stylist_pks = active_work_schedules.values_list(
                'stylist', flat=True).order_by().distinct()
        stylists = {}
        for stylist_pk in stylist_pks:
            work_scheds = active_work_schedules.filter(stylist=stylist_pk)
            stylist = work_scheds.first().stylist
            if not stylist.default_address.first() or not stylist.default_address.first().address:
                tz = _('No default address set')
            else:
                tz = get_address_timezone(
                    stylist.default_address.first().address)
            name = "{} <{}>".format(stylist.get_full_name(), stylist.email)
            stylists[stylist_pk] = {'name': name, 'email': stylist.email,
                                    'schedules': work_scheds, 'timezone': tz,
                                    'count': work_scheds.count()}
        return stylists

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        context = self.get_context_data()
        if request.is_ajax():
            data = {'html': render_to_string(
                'oscar_appointments/_work_schedules_admin_table.html',
                context_instance=RequestContext(request, context))}
            return self.render_json_response(data)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        ctx = super(StylistWorkScheduleAdminView, self).get_context_data(
            **kwargs)
        ctx['title'] = _("Stylist Work Schedules")
        ctx['form'] = self.get_form(self.form_class)
        return ctx

    def get_form_kwargs(self):
        kwargs = super(StylistWorkScheduleAdminView, self).get_form_kwargs()
        kwargs.update({
            'request': self.request
            })
        return kwargs


class PerStylistWorkScheduleAdminView(ListView):
    model = StylistWorkSchedule
    context_object_name = 'schedules'
    template_name = 'oscar_appointments/per_stylist_work_schedule_list.html'
    admin = True

    def get_queryset(self):
        pk = self.kwargs.get('pk', None)
        try:
            self.stylist = User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404
        if not self.admin and self.stylist != self.request.user:
            raise Http404
        return self.stylist.work_schedules.all()

    def get_context_data(self, **kwargs):
        ctx = super(PerStylistWorkScheduleAdminView, self).get_context_data(**kwargs)
        if self.admin:
            ctx['title'] = _("Work Schedules for Stylist: {}".format(self.stylist.get_full_name()))
        else:
            ctx['title'] = _('My Work Schedules')
        ctx['admin'] = self.admin
        ctx['stylist'] = self.stylist
        return ctx


class StylistWorkScheduleDeleteView(DeleteView):
    model = StylistWorkSchedule
    template_name = 'oscar_appointments/work_schedule_delete.html'
    context_object_name = 'schedule'

    def get_queryset(self):
        if self.request.user.is_staff:
            return StylistWorkSchedule.objects.all()
        else:
            return StylistWorkSchedule.objects.filter(stylist=self.request.user)

    def get_context_data(self, **kwargs):
        ctx = super(StylistWorkScheduleDeleteView, self).get_context_data(**kwargs)
        ctx['staff'] = self.request.user.is_staff
        ctx['title'] = _("Delete work schedule '{}: {}-{}' for stylist {}".format(
            self.object.weekday_name, self.object.start, self.object.end, self.object.stylist.get_full_name()))
        return ctx

    def get_success_url(self):
        messages.success(
            self.request, _("Work Schedule '{}: {}-{}' for stylist {} has been deleted.".format(
                self.object.weekday_name, self.object.start, self.object.end,
                self.object.stylist.get_full_name())))
        if self.request.user.is_staff:
            return reverse('schedules:admin-per-stylist-work-schedules',
                kwargs={'pk': self.object.stylist.pk})
        else:
            return reverse('schedules:work-schedules')


class StylistWorkScheduleUpdateView(UpdateView):
    model = StylistWorkSchedule
    form_class = StylistWorkScheduleForm
    template_name = 'oscar_appointments/work_schedule_form.html'

    def get_queryset(self):
        if self.request.user.is_staff:
            return StylistWorkSchedule.objects.all()
        else:
            return StylistWorkSchedule.objects.filter(stylist=self.request.user)

    def get_context_data(self, **kwargs):
        ctx = super(StylistWorkScheduleUpdateView, self).get_context_data(**kwargs)
        ctx['title'] = _("Update work schedule '{}: {}-{}' for stylist {}".format(
            self.object.weekday_name, self.object.start, self.object.end, self.object.stylist.get_full_name()))
        ctx['schedule'] = self.object
        ctx['form'] = self.get_form(self.form_class)
        ctx['stylist'] = self.object.stylist
        ctx['help_text'] = _("Any other schedules that are in conflict with the "
            "new schedules to be created will be deleted.")
        ctx['admin'] = self.request.user.is_staff
        return ctx

    def form_valid(self, form):
        start = form.cleaned_data.get('start')
        end = form.cleaned_data.get('end')
        stylist = form.cleaned_data.get('stylist')
        weekday = form.cleaned_data.get('weekday')
        self.object.start = start
        self.object.end = end
        self.object.stylist = stylist
        self.object.weekday = int(weekday)
        # need to delete StylistWorkSchedules that overlap with this schedule
        # so there's no duplicates
        StylistWorkSchedule.objects.filter(
            weekday=weekday, start__lte=end, end__gte=start,
            stylist=stylist).delete()
        self.object.save()
        return redirect(self.get_success_url())

    def get_success_url(self):
        messages.success(
            self.request, _("Work Schedule updated to '{}: {}-{}' for stylist {}.".format(
                self.object.weekday_name, self.object.start, self.object.end,
                self.object.stylist.get_full_name())))
        if self.request.user.is_staff:
            return reverse('schedules:admin-per-stylist-work-schedules',
                kwargs={'pk': self.object.stylist.pk})
        else:
            return reverse('schedules:work-schedules')


class StylistWorkScheduleCreateView(CreateView):
    model = StylistWorkSchedule
    form_class = StylistWorkScheduleCreateForm
    template_name = 'oscar_appointments/work_schedule_form.html'
    admin = False

    def get_context_data(self, **kwargs):
        ctx = super(StylistWorkScheduleCreateView, self).get_context_data(**kwargs)
        stylist = self.get_stylist()
        if self.admin:
            ctx['title'] = _("Create work schedule for stylist {}".format(
                stylist.get_full_name()))
        else:
            ctx['title'] = _("Create work schedule")
        ctx['form'] = self.get_form(self.form_class)
        ctx['stylist'] = stylist
        ctx['admin'] = self.admin
        return ctx

    def get_stylist(self, form=None):
        stylist_pk = self.kwargs.get('pk', None)
        try:
            return User.objects.get(pk=stylist_pk)
        except User.DoesNotExist:
            raise Http404
        if not self.admin and self.request.user.pk != stylist_pk:
            raise Http404

    def form_valid(self, form):
        self.stylist = self.get_stylist(form)
        start = form.cleaned_data.get('start')
        end = form.cleaned_data.get('end')
        weekdays = form.cleaned_data.get('weekdays')
        for weekday in weekdays:
            # need to delete StylistWorkSchedules that overlap with this schedule
            # so there's no duplicates
            StylistWorkSchedule.objects.filter(
                weekday=weekday, start__lte=end, end__gte=start,
                stylist=self.stylist).delete()
            StylistWorkSchedule.objects.create(weekday=int(weekday),
                start=start, end=end, stylist=self.stylist)
        return redirect(self.get_success_url())

    def get_success_url(self):
        if self.admin:
            msg = _("Please review the updated stylist work schedules.")
            url = reverse('schedules:admin-per-stylist-work-schedules',
                          kwargs={'pk': self.stylist.pk})
        else:
            msg = _("Please review your updated work schedules.")
            url = reverse('schedules:work-schedules')
        messages.success(self.request, msg)
        return url


class NewStylistWorkScheduleCreateView(StylistWorkScheduleCreateView):
    form_class = NewStylistWorkScheduleCreateForm

    def get_context_data(self, **kwargs):
        ctx = super(StylistWorkScheduleCreateView, self).get_context_data(**kwargs)
        ctx['title'] = _("Create work schedule")
        ctx['form'] = self.get_form(self.form_class)
        ctx['stylist'] = None
        ctx['admin'] = True
        return ctx

    def get_stylist(self, form=None):
        return form.cleaned_data.get('stylist')

    def get_success_url(self):
        msg = _("Please review the updated stylist work schedules.")
        url = reverse('schedules:admin-work-schedules')
        messages.success(self.request, msg)
        return url


class StylistWorkScheduleView(ListView):
    model = StylistWorkSchedule
    context_object_name = 'stylist'
    template_name = 'oscar_appointments/work_schedules.html'

    def get_queryset(self):
        pk = self.request.user
        work_scheds = StylistWorkSchedule.objects.filter(stylist=pk)
        stylist = self.request.user
        if not stylist.default_address.first() or not stylist.default_address.first().address:
            tz = _('No default address set')
        else:
            tz = get_address_timezone(stylist.default_address.first().address)
        name = "{} <{}>".format(stylist.get_full_name(), stylist.email)
        obj = {'name': name, 'email': stylist.email, 'schedules': work_scheds,
              'timezone': tz, 'count': work_scheds.count(),
              'pk': self.request.user.pk}
        return obj

    def get_context_data(self, **kwargs):
        ctx = super(StylistWorkScheduleView, self).get_context_data(
            **kwargs)
        ctx['title'] = _("My Work Schedules")
        ctx['pk'] = self.request.user.pk
        return ctx


class CurrentDatetimePaddingView(UpdateView):
    model = CurrentDateTimePadding
    fields = ('minutes',)

    def get_object(self):
        try:
            now_padding = CurrentDateTimePadding.objects.first()
        except CurrentDateTimePadding.DoesNotExist:
            now_padding = CurrentDateTimePadding.objects.create()
        return now_padding

    def get_context_data(self, **kwargs):
        ctx = super(CurrentDatetimePaddingView, self).get_context_data(
            **kwargs)
        ctx['title'] = _("Set Padding From Current Date and Time")
        return ctx

    def get_success_url(self):
        msg = _("Padding from current date and time for appointments updated "
            "to {} minutes".format(self.object.minutes))
        messages.success(self.request, msg)
        return reverse('schedules:now_padding')


class ColorListView(ListView):
    model = Color
    context_object_name = 'colors'

    def get_context_data(self, **kwargs):
        ctx = super(ColorListView, self).get_context_data(**kwargs)
        ctx['title'] = _("Stylist Colors")
        return ctx

    def get_queryset(self):
        queryset = super(ColorListView, self).get_queryset()
        queryset = queryset.order_by('stylist__first_name', 'code')
        return queryset


class ColorUpdateView(UpdateView):
    model = Color
    fields = '__all__'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form.fields['stylist'].queryset = User.active_stylists.all()
        form.fields['stylist'].label_from_instance = lambda obj: "{} <{}>".format(
            obj.get_full_name(), obj.email)
        return self.render_to_response(self.get_context_data(form=form))

    def get_context_data(self, **kwargs):
        ctx = super(ColorUpdateView, self).get_context_data(**kwargs)
        ctx['title'] = _("Edit Color with hex code {}".format(self.object.code))
        return ctx

    def get_success_url(self):
        msg = _("Color with hex code {} updated".format(self.object.code))
        messages.success(self.request, msg)
        return reverse('schedules:color-list')


class ColorCreateView(CreateView):
    model = Color
    fields = '__all__'

    def get(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form.fields['stylist'].queryset = User.active_stylists.all()
        form.fields['stylist'].label_from_instance = lambda obj: "{} <{}>".format(
            obj.get_full_name(), obj.email)
        return self.render_to_response(self.get_context_data(form=form))

    def get_context_data(self, **kwargs):
        ctx = super(ColorCreateView, self).get_context_data(**kwargs)
        ctx['title'] = _("Create Color")
        return ctx

    def get_success_url(self):
        msg = _("Color with hex code {} created".format(self.object.code))
        messages.success(self.request, msg)
        return reverse('schedules:color-list')


class AvailableSchedulesIntervalView(UpdateView):
    model = AvailableSchedulesInterval
    fields = ('minutes',)

    def get_object(self):
        try:
            interval = AvailableSchedulesInterval.objects.first()
        except AvailableSchedulesInterval.DoesNotExist:
            interval = AvailableSchedulesInterval.objects.create()
        return interval

    def get_context_data(self, **kwargs):
        ctx = super(AvailableSchedulesIntervalView, self).get_context_data(
            **kwargs)
        ctx['title'] = _("Set Available Schedules Interval")
        return ctx

    def get_success_url(self):
        msg = _("Available schedules interval updated to {} minutes".format(
            self.object.minutes))
        messages.success(self.request, msg)
        return reverse('dashboard:index')
