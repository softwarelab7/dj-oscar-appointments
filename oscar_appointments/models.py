# -*- coding: utf-8 -*-
import datetime
import pytz
from decimal import Decimal as D
from django_extensions.db.fields import UUIDField
from django.conf import settings
from django.db import models
from django.db.models import Avg
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel
from timezone_field import TimeZoneField

from oscar.core.compat import AUTH_USER_MODEL
from zs_core.codes import (CANCELLED, FINISHED, FOR_APPROVAL, STATUS_CHOICES,
                           PENDING, PAID)


class UnavailableSchedule(models.Model):
    stylist = models.ForeignKey(AUTH_USER_MODEL,
                                related_name='unavailable_scheds',
                                verbose_name=_('Stylists'))
    title = models.CharField(_('title'), max_length=256)
    start = models.DateTimeField()
    end = models.DateTimeField()
    uuid = UUIDField(unique=True)
    timezone = TimeZoneField(null=True, blank=True)

    class Meta:
        verbose_name = _('Unavailable Schedule')
        ordering = ['-start']

    def user_is_stylist(self, user):
        return self.stylist == user

    def __unicode__(self):
        return u"%s [%s-%s]" % (self.title, self.start.strftime("%c"), self.end.strftime("%c"))


class Color(models.Model):
    code = models.CharField(_('code'), max_length=7, unique=True,
        help_text=_("Hexadecimal color code with format: '#XXXXXX'"))
    stylist = models.OneToOneField(AUTH_USER_MODEL, related_name='color',
        blank=True, null=True)

    def __str__(self):
        return "{} ({})".format(self.code, getattr(self, 'stylist', None))


class GratuityQuerySet(models.QuerySet):
    def eligible(self, until=None):
        if until is None:
            until = datetime.datetime.now(pytz.utc)
        two_hours_before_until = until - datetime.timedelta(hours=2)
        return self.exclude(end__gt=two_hours_before_until).exclude(
            status=CANCELLED).filter(gratuities__status=PENDING).distinct()


class RatingQuerySet(models.QuerySet):
    def eligible(self, until=None, within=None):
        # default until is day before
        # default within is a week
        if until is None:
            until = datetime.datetime.now(pytz.utc) - datetime.timedelta(days=1)
        if within is None:
            within = datetime.datetime.now(pytz.utc) - datetime.timedelta(days=7)
        return self.filter(status=FINISHED, end__lte=until, end__gt=within, ratings__isnull=True)


class Appointment(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    start = models.DateTimeField()
    end = models.DateTimeField()
    padded_start = models.DateTimeField()
    padded_end = models.DateTimeField()
    look = models.ForeignKey('catalogue.Product')
    style = models.ForeignKey('catalogue.Category')
    payment_method = models.ForeignKey('payment.Bankcard')
    order = models.ForeignKey('order.Order')
    stylists = models.ManyToManyField(AUTH_USER_MODEL, related_name='appointments',
                                      verbose_name=_('Stylists'))
    status = models.CharField(
        choices=STATUS_CHOICES, default=STATUS_CHOICES[0][0],
        max_length=20, verbose_name=_("Status"))
    cancellation_datetime = models.DateTimeField(blank=True, null=True)
    uuid = UUIDField(unique=True)
    address = models.ForeignKey('address.UserAddress', null=True, blank=True)
    preferred_stylist = models.TextField(blank=True, null=True)
    hair_condition = models.TextField(blank=True, null=True)
    additional_info = models.TextField(blank=True, null=True)
    amount_refunded = models.DecimalField(
        _("Amount Refunded"), decimal_places=2, max_digits=12, null=True,
        blank=True)
    timezone = TimeZoneField(null=True, blank=True)
    #TODO: must track multiple devices as well..
    four_hours_prior_notification_sent = models.DateTimeField(null=True, blank=True)
    one_hour_prior_notification_sent = models.DateTimeField(null=True, blank=True)
    started_notification_sent = models.DateTimeField(null=True, blank=True)
    rating_notification_sent = models.DateTimeField(null=True, blank=True)
    day_prior_email_sent = models.DateTimeField(null=True, blank=True)
    day_prior_notification_sent = models.DateTimeField(null=True, blank=True)
    gratuity_included = models.BooleanField(default=True)
    gratuity_percentage = models.IntegerField(default=70)

    pipeline = getattr(settings, 'APPOINTMENT_STATUS_PIPELINE', {})

    objects = models.Manager()
    rate_qs = RatingQuerySet.as_manager()
    gratuity = GratuityQuerySet.as_manager()

    @property
    def resolved_stylists(self):
        return self.stylists.all()

    # @property
    # def rating(self):
    #     rating = self.ratings.aggregate(Avg('value'))['value__avg']
    #     if rating:
    #         rating = D(rating).quantize(D('0.1'))
    #     return rating

    def user_is_stylist(self, user):
        return user in self.stylists.all()

    def available_statuses(self):
        """
        Return all possible statuses that this appointment can move to
        """
        return self.pipeline.get(self.status, ())

    def is_refundable(self):
        # appointment is only refundable if cancelled up to 6 hours before the
        # booking appointment
        now = datetime.datetime.now(pytz.utc)
        start = self.start
        if (start - now).total_seconds()/3600 < 6:
            return False
        else:
            return True

    def get_date_created(self):
        return self.order.date_placed

    def active_additional_charges(self):
        return self.additional_charges.filter(status__in=[PENDING, FOR_APPROVAL]).order_by('status', '-date_created')

    def __str__(self):
        return "%s [%s-%s]" % (self.look.title, self.start.strftime("%c"), self.end.strftime("%c"))

WEEKDAYS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
            'Saturday', 'Sunday']


class StylistWorkSchedule(models.Model):
    stylist = models.ForeignKey(AUTH_USER_MODEL, related_name='work_schedules',
                                verbose_name=_('Stylists'))
    # store as integer since weekday() returns day of the week as an integer,
    # where Monday is 0 and Sunday is 6.
    weekday = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(6)])
    start = models.TimeField()
    end = models.TimeField()
    uuid = UUIDField(unique=True)

    class Meta:
        ordering = ['weekday', 'start']

    def __unicode__(self):
        return "{}: {} {}-{}".format(self.stylist.get_full_name(),
                                     self.weekday_name, self.start, self.end)

    @property
    def weekday_name(self):
        return(WEEKDAYS[self.weekday])


class AppointmentPadding(SingletonModel):
    """
    use this for global padding setting
    """
    minutes = models.PositiveIntegerField(
        _('Minutes'), help_text=_("padding before and after appointment in minutes"), default=15)


class CurrentDateTimePadding(SingletonModel):
    minutes = models.PositiveIntegerField(
        _("Minutes"), help_text=_("padding in minutes from current datetime "
        "for booking appointments"), default=60)


class AvailableSchedulesInterval(SingletonModel):
    minutes = models.PositiveIntegerField(_("Minutes"), help_text=_(
        "interval in minutes for start times in available schedules"),
        default=15, validators=[MinValueValidator(1)])
