from django.contrib import admin
from .models import Appointment, Color, StylistWorkSchedule, UnavailableSchedule


class AppointmentAdmin(admin.ModelAdmin):
    search_fields = ['order__number']
    list_filter = ['gratuity_included', 'status']


class StylistWorkScheduleAdmin(admin.ModelAdmin):
    search_fields = ['stylist__email', 'stylist__first_name', 'stylist__last_name', 'stylist__username']


class UnavailableScheduleAdmin(admin.ModelAdmin):
    search_fields = ['stylist__email', 'stylist__first_name', 'stylist__last_name', 'stylist__username']


class ColorAdmin(admin.ModelAdmin):
    search_fields = ['stylist__email', 'stylist__first_name', 'stylist__last_name', 'stylist__username']


admin.site.register(Appointment, AppointmentAdmin)
admin.site.register(Color, ColorAdmin)
admin.site.register(StylistWorkSchedule, StylistWorkScheduleAdmin)
admin.site.register(UnavailableSchedule, UnavailableScheduleAdmin)
