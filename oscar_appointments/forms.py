import datetime
import pytz

from django import forms
from django.utils.timezone import get_current_timezone
from django.utils.translation import ugettext_lazy as _

from oscar.core.compat import get_user_model
from oscar.forms import widgets

from .models import AppointmentPadding, StylistWorkSchedule
from dj_address_utils.utils import (get_address_timezone,
                                    get_default_address_timezone)
from zs_core.fields import UserModelChoiceField
from zs_core.utils import get_form_tzinfo


User = get_user_model()


class AddUnavailableScheduleForm(forms.Form):
    title = forms.CharField(max_length=256)
    start_date = forms.DateField(required=True, widget=widgets.DatePickerInput)
    start_time = forms.TimeField(
        required=True,
        widget=widgets.TimePickerInput(format='%H:%M'),
        label=_('Start Time')
    )
    end_date = forms.DateField(required=True, widget=widgets.DatePickerInput)
    end_time = forms.TimeField(
        required=True,
        widget=widgets.TimePickerInput(format='%H:%M'),
        label=_('End Time')
    )

    def __init__(self, *args, **kwargs):
        self.default_address = kwargs.pop('default_address', None)
        super(AddUnavailableScheduleForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(AddUnavailableScheduleForm, self).clean()
        # require default address, so unavailable schedule is always in the 
        # right timezone
        if not self.default_address:
            raise forms.ValidationError(_("There is no set default address."))
        # !! clean still gets called even if there are errors in other fields
        # still need to check that these all exist
        start_date = cleaned_data.get('start_date', None)
        start_time = cleaned_data.get('start_time', None)
        end_date = cleaned_data.get('end_date', None)
        end_time = cleaned_data.get('end_time', None)
        if (start_date is None or start_time is None or end_date is None or end_time is None):
            return cleaned_data
        start = datetime.datetime.combine(start_date, start_time)
        # use tzinfo from stylist default address
        timezone = get_default_address_timezone(self.default_address)
        if not timezone:
            raise forms.ValidationError(_("Unable to get the timezone for"
                "the default address. Please try again later."))
        start = timezone.localize(start)
        end = datetime.datetime.combine(end_date, end_time)
        end = timezone.localize(end)
        if (end <= start):
            raise forms.ValidationError(_("The end date and time of the "
                "unavailable schedule must be after the start date and time"))
        else:
            cleaned_data['start'] = start
            cleaned_data['end'] = end
        return cleaned_data


class AppointmentPaddingForm(forms.ModelForm):
    model = AppointmentPadding


class TimezoneForm(forms.Form):
    timezone = forms.ChoiceField(choices=[(x, x) for x in pytz.common_timezones],
                                 widget=forms.Select(attrs={'class': 'no-widget-init'}))

    def __init__(self, *args, **kwargs):
        timezone = kwargs.pop('timezone', None)
        super(TimezoneForm, self).__init__(*args, **kwargs)
        if timezone:
            self.fields['timezone'].initial = timezone
        else:
            self.fields['timezone'].initial = get_current_timezone()


class StylistWorkScheduleAdminForm(forms.Form):
    stylist = UserModelChoiceField(
        queryset=User.objects.none(),
        widget=forms.Select(attrs={'class': 'no-widget-init'}))

    def __init__(self, request, *args, **kwargs):
        super(StylistWorkScheduleAdminForm, self).__init__(*args, **kwargs)
        active_stylists = User.active_stylists.all()
        stylist_pks = StylistWorkSchedule.objects.filter(
            stylist__in=active_stylists).values_list('stylist', flat=True)
        stylist_set = User.objects.filter(pk__in=stylist_pks)
        self.fields['stylist'].queryset = stylist_set.exclude(
            id=request.user.id)
        if 'stylist_id' in request.session:
            self.fields['stylist'].initial = request.session['stylist_id']


class StylistWorkScheduleForm(forms.ModelForm):
    start = forms.TimeField(
        required=True,
        widget=widgets.TimePickerInput(format='%H:%M'),
        label=_('Start')
    )
    end = forms.TimeField(
        required=True,
        widget=widgets.TimePickerInput(format='%H:%M'),
        label=_('End')
    )
    weekday = forms.ChoiceField(choices=[
        (0, 'Monday'), (1, 'Tuesday'), (2, 'Wednesday'), (3, 'Thursday'),
        (4, 'Friday'), (5, 'Saturday'), (6, 'Sunday')])

    class Meta:
        model = StylistWorkSchedule
        widgets = {'stylist': forms.HiddenInput()}
        fields = ('start', 'end', 'weekday', 'stylist')


class StylistWorkScheduleCreateForm(forms.Form):
    start = forms.TimeField(
        required=True,
        widget=widgets.TimePickerInput(format='%H:%M'),
        label=_('Start')
    )
    end = forms.TimeField(
        required=True,
        widget=widgets.TimePickerInput(format='%H:%M'),
        label=_('End')
    )
    weekdays = forms.MultipleChoiceField(choices=[
        (0, 'Monday'), (1, 'Tuesday'), (2, 'Wednesday'), (3, 'Thursday'),
        (4, 'Friday'), (5, 'Saturday'), (6, 'Sunday')])

    def __init__(self, instance, *args, **kwargs):
        super(StylistWorkScheduleCreateForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        start = cleaned_data.get('start', None)
        end = cleaned_data.get('end', None)
        if start is None or end is None:
            return cleaned_data
        if (end <= start):
            raise forms.ValidationError(_("The end time must be after the start time"))
        return cleaned_data


class NewStylistWorkScheduleCreateForm(StylistWorkScheduleCreateForm):
    stylist = UserModelChoiceField(
        queryset=User.objects.none(),
        widget=forms.Select(attrs={'class': 'no-widget-init'}))

    def __init__(self, *args, **kwargs):
        super(NewStylistWorkScheduleCreateForm, self).__init__(*args, **kwargs)
        self.fields['stylist'].queryset = User.active_stylists.all()

    def clean(self):
        cleaned_data = self.cleaned_data
        start = cleaned_data.get('start', None)
        end = cleaned_data.get('end', None)
        if start is None or end is None:
            return cleaned_data
        if (end <= start):
            raise forms.ValidationError(_("The end time must be after the start time"))
        return cleaned_data
