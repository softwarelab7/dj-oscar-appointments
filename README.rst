=============================
Djange Oscar Appointments
=============================

.. image:: https://badge.fury.io/py/dj-oscar-appointments.png
    :target: https://badge.fury.io/py/dj-oscar-appointments

.. image:: https://travis-ci.org/aidzz/dj-oscar-appointments.png?branch=master
    :target: https://travis-ci.org/aidzz/dj-oscar-appointments

Your project description goes here

Documentation
-------------

The full documentation is at https://dj-oscar-appointments.readthedocs.org.

Quickstart
----------

Install Djange Oscar Appointments::

    pip install dj-oscar-appointments

Then use it in a project::

    import oscar_appointments

Features
--------

* TODO

Running Tests
--------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install -r requirements_test.txt
    (myenv) $ python runtests.py

Credits
---------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
