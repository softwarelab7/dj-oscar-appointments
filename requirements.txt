django>=1.9.6

# Additional requirements go here
django-solo==1.1.2
django-oscar==1.3
-e git+https://aidz@bitbucket.org/softwarelab7/zs-core-utils.git#egg=zs_core
